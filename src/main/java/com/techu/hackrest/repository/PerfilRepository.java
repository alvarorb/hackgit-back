package com.techu.hackrest.repository;

import com.techu.hackrest.model.PerfilModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfilRepository extends MongoRepository<PerfilModel, String> {



}
