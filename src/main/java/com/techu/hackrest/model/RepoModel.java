package com.techu.hackrest.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "repos")

public class RepoModel {

    @Id
    private String id;
    @NotNull
    private String repoName;
    private String urlg;

    public RepoModel(){}

    public RepoModel(String id, String repoName, String urlg) {
        this.id = id;
        this.repoName = repoName;
        this.urlg = urlg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getUrlg() {
        return urlg;
    }

    public void setUrlg(String urlg) {
        this.urlg = urlg;
    }
}
