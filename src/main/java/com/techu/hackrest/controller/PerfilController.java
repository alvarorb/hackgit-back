package com.techu.hackrest.controller;

import com.techu.hackrest.model.PerfilModel;
import com.techu.hackrest.service.perfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
@RequestMapping("/hackgit/v1")
public class PerfilController {

    @Autowired
    perfilService perfilService;

    @GetMapping("/perfiles")
    public List<PerfilModel> getPerfiles() {
        return perfilService.findAll();
    }

    @GetMapping("/perfiles/{id}")
    public Optional<PerfilModel> getPerfilId(@PathVariable String id){
        return perfilService.findById(id);
    }

    @PostMapping("/perfiles")
    public PerfilModel postPerfiles(@RequestBody PerfilModel newPerfil){
        perfilService.save(newPerfil);
        return newPerfil;
    }

    @PutMapping("/perfiles")
    public boolean putPerfiles(@RequestBody PerfilModel perfilToUpdate){
        if (perfilService.existsByID(perfilToUpdate.getId())){
            perfilService.save(perfilToUpdate);
            return true;
        } else {
                return false;
        }
    }

    @DeleteMapping("/perfiles")
    public boolean deletePerfiles(@RequestBody PerfilModel perfilToDelete) {
        if (perfilService.existsByID(perfilToDelete.getId())){
            return perfilService.delete(perfilToDelete);
        } else {
            return false;
        }
    }
    @DeleteMapping("/perfiles/{id}")
    public boolean deletePerfilesById(@PathVariable String id) {

        return perfilService.deleteById(id);
    }

}
