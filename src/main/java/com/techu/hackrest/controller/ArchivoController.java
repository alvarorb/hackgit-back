package com.techu.hackrest.controller;

import com.techu.hackrest.service.perfilService;
import com.techu.hackrest.service.repoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
@RequestMapping("/hackgit/v1")
public class ArchivoController {

    @Autowired
    repoService repoService;

    @Autowired
    perfilService perfilService;

    @RequestMapping("/archivos/{idPerfil}/{idRepo}/{idRepo2}/{tipof}")
    public ResponseEntity<String> creaFichero(@PathVariable String idPerfil, @PathVariable String idRepo, @PathVariable String idRepo2, @PathVariable String tipof)
    {
        String cuerpo="";
        if (tipof=="sh") {

            cuerpo = "#!/bin/bash" + "\n";
            cuerpo = cuerpo + "nombre=" + perfilService.findById(idPerfil).get().getNombre() + "\n";
            cuerpo = cuerpo + "correo=" + perfilService.findById(idPerfil).get().getCorreo() + "\n";
            cuerpo = cuerpo + "repoName=" + repoService.findById(idRepo).get().getRepoName() + "\n";
            cuerpo = cuerpo + "urlg=" + repoService.findById(idRepo).get().getUrlg() + "\n";
            cuerpo = cuerpo + "repoName2=" + repoService.findById(idRepo2).get().getRepoName() + "\n";
            cuerpo = cuerpo + "urlg2=" + repoService.findById(idRepo2).get().getUrlg() + "\n";

            cuerpo = cuerpo + "echo descargando el repositorio $repoName \n";
            cuerpo = cuerpo + "echo git clone $urlg \n";
            cuerpo = cuerpo + "git clone $urlg \n";
            cuerpo = cuerpo + "echo \n";

            cuerpo = cuerpo + "echo accediendo a la carpeta del repositorio $repoName descargado \n";
            cuerpo = cuerpo + "echo cd $repoName\n";
            cuerpo = cuerpo + "cd $repoName\n";
            cuerpo = cuerpo + "echo \n";

            cuerpo = cuerpo + "echo definiendo la variable name\n";
            cuerpo = cuerpo + "echo git config --global user.name $nombre \n";
            cuerpo = cuerpo + "git config --global user.name $nombre \n";
            cuerpo = cuerpo + "echo \n";

            cuerpo = cuerpo + "echo definiendo la variable email\n";
            cuerpo = cuerpo + "echo git config --global user.email $correo \n";
            cuerpo = cuerpo + "git config --global user.email $correo \n";
            cuerpo = cuerpo + "echo \n";

            cuerpo = cuerpo + "echo iniciando git \n";
            cuerpo = cuerpo + "echo git init \n";
            cuerpo = cuerpo + "git init\n";
            cuerpo = cuerpo + "echo \n";

            cuerpo = cuerpo + "echo enlazando la carpeta al repositorio destino $repoName2 \n";
            cuerpo = cuerpo + "echo git remote rm origin \n";
            cuerpo = cuerpo + "git remote rm origin \n";
            cuerpo = cuerpo + "echo git remote add origin $urlg2\n";
            cuerpo = cuerpo + "git remote add origin $urlg2\n";
            cuerpo = cuerpo + "echo \n";
        }
        else {

            cuerpo = "@echo off" + "\n";
            cuerpo = cuerpo + "set nombre=" + perfilService.findById(idPerfil).get().getNombre() + "\n";
            cuerpo = cuerpo + "set correo=" + perfilService.findById(idPerfil).get().getCorreo() + "\n";
            cuerpo = cuerpo + "set repoName=" + repoService.findById(idRepo).get().getRepoName() + "\n";
            cuerpo = cuerpo + "set urlg=" + repoService.findById(idRepo).get().getUrlg() + "\n";
            cuerpo = cuerpo + "set repoName2=" + repoService.findById(idRepo2).get().getRepoName() + "\n";
            cuerpo = cuerpo + "set urlg2=" + repoService.findById(idRepo2).get().getUrlg() + "\n";

            cuerpo = cuerpo + "echo descargando el repositorio %repoName% \n";
            cuerpo = cuerpo + "echo git clone %urlg% \n";
            cuerpo = cuerpo + "git clone %urlg% \n";


            cuerpo = cuerpo + "echo accediendo a la carpeta del repositorio %repoName% descargado \n";
            cuerpo = cuerpo + "echo cd %repoName%\n";
            cuerpo = cuerpo + "cd %repoName%\n";


            cuerpo = cuerpo + "echo definiendo la variable name\n";
            cuerpo = cuerpo + "echo git config --global user.name %nombre% \n";
            cuerpo = cuerpo + "git config --global user.name %nombre% \n";


            cuerpo = cuerpo + "echo definiendo la variable email\n";
            cuerpo = cuerpo + "echo git config --global user.email %correo% \n";
            cuerpo = cuerpo + "git config --global user.email %correo% \n";


            cuerpo = cuerpo + "echo iniciando git \n";
            cuerpo = cuerpo + "echo git init \n";
            cuerpo = cuerpo + "git init\n";


            cuerpo = cuerpo + "echo enlazando la carpeta al repositorio destino %repoName2% \n";
            cuerpo = cuerpo + "echo git remote rm origin \n";
            cuerpo = cuerpo + "git remote rm origin \n";
            cuerpo = cuerpo + "echo git remote add origin %urlg2% \n";
            cuerpo = cuerpo + "git remote add origin %urlg2% \n";



        }

        return (ResponseEntity<String>) ResponseEntity.status(HttpStatus.CREATED)
                .header("Content-Disposition", "attachment; filename=\"hackgit."+tipof+"\"")
                .body(cuerpo);
    }

}
