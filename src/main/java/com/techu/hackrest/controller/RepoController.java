package com.techu.hackrest.controller;

import com.techu.hackrest.model.RepoModel;
import com.techu.hackrest.service.repoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
@RequestMapping("/hackgit/v1")
public class RepoController {

    @Autowired
    repoService repoService;

    @GetMapping("/repos")
    public List<RepoModel> getRepos() {
        return repoService.findAll();
    }

    @GetMapping("/repos/{id}")
    public Optional<RepoModel> getRepoId(@PathVariable String id){
        return repoService.findById(id);
    }

    @PostMapping("/repos")
    public RepoModel postRepos(@RequestBody RepoModel newRepo){
        repoService.save(newRepo);
        return newRepo;
    }

    @PutMapping("/repos")
    public boolean putRepos(@RequestBody RepoModel repoToUpdate){
        if (repoService.existsByID(repoToUpdate.getId())){
            repoService.save(repoToUpdate);
            return true;
        } else {
                return false;
        }
    }

    @DeleteMapping("/repos")
    public boolean deleteRepos(@RequestBody RepoModel repoToDelete) {
        if (repoService.existsByID(repoToDelete.getId())){
            return repoService.delete(repoToDelete);
        } else {
            return false;
        }
    }
    @DeleteMapping("/repos/{id}")
    public boolean deleteReposById(@PathVariable String id) {

        return repoService.deleteById(id);
    }

}
